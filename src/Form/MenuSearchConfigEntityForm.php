<?php

namespace Drupal\menus_search\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class MenuSearchConfigEntityForm.
 */
class MenuSearchConfigEntityForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $menu_search_config_entity = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $menu_search_config_entity->label(),
      '#description' => $this->t("Label for the Menu search config."),
      '#required' => TRUE,
    ];

    $menus = \Drupal::entityTypeManager()->getStorage('menu')->loadMultiple();
    $menu_options = [];
    foreach ($menus as $menu) {
      $menu_options[$menu->id()] = $menu->label();
    }

    $form['menus'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select the menus'),
      '#description' => $this->t('Select the menus to be searched by users.'),
      '#options' => $menu_options,
      '#default_value' => $menu_search_config_entity->get('menus'),
    ];

    $roles = user_role_names();

    $form['roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select the roles'),
      '#description' => $this->t('Select the roles for whom menu search should be available.'),
      '#options' => $roles,
      '#default_value' => $menu_search_config_entity->get('roles'),
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $menu_search_config_entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\menus_search\Entity\MenuSearchConfigEntity::load',
      ],
      '#disabled' => !$menu_search_config_entity->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $menu_search_config_entity = $this->entity;
    $status = $menu_search_config_entity->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Menu search config.', [
          '%label' => $menu_search_config_entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Menu search config.', [
          '%label' => $menu_search_config_entity->label(),
        ]));
    }
    $form_state->setRedirectUrl($menu_search_config_entity->toUrl('collection'));
  }

}
