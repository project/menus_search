<?php

namespace Drupal\menus_search\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Menu search config entities.
 */
interface MenuSearchConfigEntityInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
