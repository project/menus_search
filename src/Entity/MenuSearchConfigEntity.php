<?php

namespace Drupal\menus_search\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Menu search config entity.
 *
 * @ConfigEntityType(
 *   id = "menu_search_config_entity",
 *   label = @Translation("Menu search config"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\menus_search\MenuSearchConfigEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\menus_search\Form\MenuSearchConfigEntityForm",
 *       "edit" = "Drupal\menus_search\Form\MenuSearchConfigEntityForm",
 *       "delete" = "Drupal\menus_search\Form\MenuSearchConfigEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\menus_search\MenuSearchConfigEntityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "menu_search_config_entity",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/search/menu_search_config_entity/{menu_search_config_entity}",
 *     "add-form" = "/admin/config/search/menu_search_config_entity/add",
 *     "edit-form" = "/admin/config/search/menu_search_config_entity/{menu_search_config_entity}/edit",
 *     "delete-form" = "/admin/config/search/menu_search_config_entity/{menu_search_config_entity}/delete",
 *     "collection" = "/admin/config/search/menu_search_config_entity"
 *   }
 * )
 */
class MenuSearchConfigEntity extends ConfigEntityBase implements MenuSearchConfigEntityInterface {

  /**
   * The Menu search config ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Menu search config label.
   *
   * @var string
   */
  protected $label;

  /**
   * The menu search list.
   *
   * @var array
   */
  protected $menus;

  /**
   * Roles, who has access to this menu search.
   */
  protected $roles;

}
